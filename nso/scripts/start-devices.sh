# source ncsrc file
source /opt/ncs/current/ncsrc

# start netsim devices
ncs-netsim start --dir /netsim

# This call hangs until ncsd is completely started
#  It will keep trying the initial connection to ncsd for at most TryTime seconds
ncs --wait-started 30

# Load merge authgorups.xml file
ncs_load -l -m /tmp/load_files/authgroups.xml

# Load merge device.xml file
ncs_load -l -m /netsim/devices.xml

# fetch ssh host-keys
echo "devices device * ssh fetch-host-keys" | ncs_cli -u admin

# sync-from the devices
echo "devices device * sync-from" | ncs_cli -u admin