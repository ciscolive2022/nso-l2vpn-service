#!/bin/sh

CONF_FILE=${CONF_FILE:-/etc/ncs/ncs.conf}
INSTALL_DIR=${INSTALL_DIR:-/tmp/nso-packages}


echo "Adding REST-API explorer"
mkdir -p $INSTALL_DIR
cd $INSTALL_DIR
git clone https://gitlab.com/nso-developer/rest-api-explorer.git
cp rest-api-explorer/rest-api-explorer.tar.gz /var/opt/ncs/packages/

echo "Tweaking ncs.conf for rest-api-explorer"
xmlstarlet edit --inplace -N x=http://tail-f.com/yang/tailf-ncs-config \
           -s '/x:ncs-config/x:restconf' -t elem -n require-module-name \
           -s '/x:ncs-config/x:restconf/require-module-name' -t elem -n enabled -v false \
           $CONF_FILE

HEADER_VALUE="default-src 'self';
        font-src 'self' data:;
        img-src 'self' data:;
        style-src 'self' 'unsafe-inline' 'unsafe-eval';
        script-src 'self' 'unsafe-inline' 'unsafe-eval';
        "
xmlstarlet edit --inplace -N x=http://tail-f.com/yang/tailf-ncs-config \
           -s '/x:ncs-config/x:webui' -t elem -n custom-headers \
           -s '/x:ncs-config/x:webui/custom-headers' -t elem -n header \
           -s '/x:ncs-config/x:webui/custom-headers/header' -t elem -n name -v 'Content-Security-Policy' \
           -s '/x:ncs-config/x:webui/custom-headers/header' -t elem -n value -v "${HEADER_VALUE}" \
           $CONF_FILE