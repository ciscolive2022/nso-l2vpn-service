# NSO L2VPN Service

This repo is used to build a docker image of NSO containing a simple L2VPN service used in CiscoLive DevNet Workshop (DEVWKS-3984)

## Folder structure

.
└── nso
    ├── config             - NSO conficuration files, such as authgroups.xml
    ├── ned-packages       - NEDs to be included in the image
    ├── nso-install-files  - NSO installation file
    ├── packages           - Service packagees to be included in the image
    └── scripts            - Additional scripts ot run pre-, post- or during container startup 

## Image build procedure

1. Clone the repository
```
git clone https://gitlab.com/ciscolive2022/nso-l2vpn-service.git
```
2. Download NSO installation binary into nso-install-files folder
3. Download necessary NED installation files into ned-packages folder


## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/ciscolive2022/nso-l2vpn-service/-/settings/integrations)

